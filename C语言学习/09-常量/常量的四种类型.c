#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>

#define MAX 10;

enum Gender {
	MALE=3,
	FEMALE,
	secret
};
int main()
{
	//1.字面常量
	3.14;  10; 'a';

	//2.const修饰的常变量
	/*const int num = 10;
	num = 20;
	printf("num=%d\n", num);*/

	//int arr[10] = { 0 };

	//const int n = 10;//const修饰的是常变量
	//int arr2[n] = { 0 };//n是变量  这里是不行的

	//3.define的标识符常量
	int n = MAX;
	printf("n=%d\n", n);

	//4.枚举
	enum Gender g = MALE;
	printf("%d\n", MALE);
	printf("%d\n", FEMALE);
	return 0;
}

